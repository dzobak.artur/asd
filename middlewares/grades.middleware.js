const createError = require('http-errors');

const gradeService = require('../services/grades.service');
const { GradeCreateSchema, GradeUpdateSchema } = require('../joi_validation_schemas/grades.schemas');

const gradeCreationDataValidation = async (req, res, next) => {
    try {
        const { grade } = req.body;
        const { error } = GradeCreateSchema.validate(req.body);
        if (error) {
            throw createError.BadRequest(error.details[0].message);
        }

        const existingGrade = await gradeService.findOne({
            $or: [
                { ticketNumber: req.body.ticketNumber },
                { subject: req.body.subject },
            ]
        });

        if (existingGrade) {
            throw createError.BadRequest("User with such ticketNumber or subject already exist");
        }

        if (grade < 0 || grade > 100) {
            throw createError.BadRequest("Оцінка має бути в межах від 0 до 100");
        }

        next();
    } catch (err) {
        next(err);
    }
};

async function gradesGreaterThanSixty(req, res, next) {
    try {
        const grades = await gradeService.fetchGrades({}); 

        const filteredGrades = grades.items.filter(grade => grade.grade > 60);

        const selectedGrades = filteredGrades.map(grade => ({
            lastName: grade.lastName,
            group: grade.group,
            subject: grade.subject,
            ticketNumber: grade.ticketNumber,
            grade: grade.grade,
            teacher: grade.teacher
        }));

        res.status(200).json({
            status: 200,
            count: selectedGrades.length,
            data: {
                items: selectedGrades,
                
            }
        });
    } catch(err) {
        next(err);
    }
}

module.exports = {

    gradesGreaterThanSixty,
    gradeCreationDataValidation
    
};