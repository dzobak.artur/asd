const userService = require("../services/users.service");
const authService = require("../services/auth.service");
const createError = require("http-errors");
const bcrypt = require("bcrypt");
const { verifyUserPermissions } = require('../authhelper/authhelper');


async function createUser(req, res, next) {
  try {
    const _id = await userService.create({
      ...req.body,
      password: await bcrypt.hash(req.body.password, await bcrypt.genSalt(10)),
      
    });

    res.status(200).json({
      status: 200,
      data: { _id },
    });
  } catch (err) {
    next(createError.InternalServerError(err.message));
  }
}

async function getUsers(req, res, next) {
  try {
    res.status(200).json({
      status: 200,
      data: await userService.find({}),
    });
  } catch (err) {
    next(createError.InternalServerError(err.message));
  }
}

async function getUser(req, res, next) {
  try {
    const { userId } = req.params;
    const user = await userService.findById(userId);

    if (!user) {
      return res.status(404).json({
        status: 404,
        error: {
          message: "КОристувача не знайдено",
        },
      });
    }

    res.status(200).json({
      status: 200,
      data: user,
    });
  } catch (err) {
    next(createError.InternalServerError(err.message));
  }
}
async function updateUser(req, res, next) {
  try {
    const { isValid, status, message } = await verifyUserPermissions(req);
    if (!isValid) {
      return res.status(status).json({
        status,
        error: {
          message,
        },
      });
    }
    const { userId } = req.params;
    const userData = req.body;
    await userService.findByIdAndUpdate(userId, userData);
    
    res.status(200).json({
      status: 200,
    });
  } catch (err) {
    next(createError.InternalServerError(err.message));
  }
}
  
async function deleteUser(req, res, next) {
  try {
    const { isValid, status, message } = await verifyUserPermissions(req);
    
    if (!isValid) {
      return res.status(status).json({
        status,
        error: {
          message,
        },
      });
    }

    const { userId } = req.params;
    await userService.findByIdAndDelete(userId);
    
    res.status(200).json({
      status: 200,
    });
  } catch (err) {
    next(createError.InternalServerError(err.message));
  }
}

module.exports = {
  createUser,
  getUsers,
  getUser,
  updateUser,
  deleteUser,
};