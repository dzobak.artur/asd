const authService = require('../services/auth.service');

async function verifyUserPermissions(req) {
    const { userId } = req.params;
    const tokenData = await authService.verifyAccessToken(req.headers['x-auth']);
    
    if (tokenData.id !== userId) {
      return {
        isValid: false,
        status: 403,
        message: 'У вас немає прав для редагування або видалення',
      };
    }
  
    return { isValid: true };
  }

module.exports = {
  verifyUserPermissions,
};
